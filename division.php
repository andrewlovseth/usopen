<?php

/*

	Template Name: Division

*/


get_header(); ?>

	<section id="hero" class="hero-image" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
		<div class="wrapper">

			<div class="info">
				<h2>
					<span><?php $parentID = wp_get_post_parent_id($post->ID); the_field('event_name', $parentID); ?></span>
				</h2>
				<h1>
					<span><?php the_title(); ?></span>
				</h1>
			</div>

		</div>
	</section>

	<section id="main">
		<div class="wrapper">

			<section id="schedule-results">

				<h3 class="section-heading">Schedule & Results</h3>

				<div id="tab-links">
					<a href="#pool-play">Pool Play</a>
					<a href="#bracket">Bracket</a>
				</div>

				<div id="tabs">

					<article id="pool-play">
						<?php if(get_field('score_reporter_link')): ?>
							<a href="<?php the_field('score_reporter_link'); ?>" class="score-reporter">Full Results on Score Reporter</a>
						<?php endif; ?>

						<?php if(have_rows('pools')): while(have_rows('pools')) : the_row(); ?>
						 
						    <?php if( get_row_layout() == 'pool' ): ?>
								
								<div class="pool">
						    		<h4><?php the_sub_field('pool_name'); ?></h4>

						    		<div class="standings">

						    			<h5>Standings</h5>

						    			<div class="header">
						    				<span class="team">Team</span>
						    				<span class="win-loss">W-L</span>
						    				<span class="tie">Tie</span>
						    			</div>
										
										<?php if(have_rows('standings')): while(have_rows('standings')): the_row(); ?>
										 
							    			<div class="row">
							    				<span class="team">
													<?php $post_object = get_sub_field('team'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
														<a href="<?php the_permalink(); ?>"><?php the_field('school'); ?> (<?php the_field('seed'); ?>)</a>
													<?php wp_reset_postdata(); endif; ?>
												</span>
							    				<span class="win-loss"><?php the_sub_field('record'); ?></span>
							    				<span class="tie"><?php the_sub_field('plus_minus'); ?></span>
							    			</div>

										<?php endwhile; endif; ?>

						    		</div>

						    		<div class="games">

						    			<h5>Scores</h5>

										<?php if(have_rows('games')): while(have_rows('games')): the_row(); ?>
										 
											<div class="result">
												<div class="time">
													<?php if(get_sub_field('date') !== ''): ?>
														<?php the_sub_field('date'); ?>
													<?php endif; ?>
												</div>

												<div class="match-up">
													<span class="team-a">
														<?php $post_object = get_sub_field('team_a'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
															<a href="<?php the_permalink(); ?>"><?php the_field('school'); ?> (<?php the_field('seed'); ?>)</a>
														<?php wp_reset_postdata(); endif; ?>

													</span>
													<span class="versus"><?php the_sub_field('team_a_score'); ?> - <?php the_sub_field('team_b_score'); ?></span>
													<span class="team-b">
														<?php $post_object = get_sub_field('team_b'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
															<a href="<?php the_permalink(); ?>"><?php the_field('school'); ?> (<?php the_field('seed'); ?>)</a>
														<?php wp_reset_postdata(); endif; ?>
													</span>
											    </div>

												<div class="link">
													<?php if(get_sub_field('field') !== ''): ?>
														Field <?php the_sub_field('field'); ?>
													<?php endif; ?>
												</div>
											</div>

										<?php endwhile; endif; ?>

						    		</div>

								</div>
								
						    <?php endif; ?>


						    <?php if( get_row_layout() == 'crossover_games' ): ?>
								
								<div class="pool">
						    		<h4><?php the_sub_field('pool_name'); ?></h4>

						    		<div class="games">

						    			<h5>Scores</h5>

										<?php if(have_rows('games')): while(have_rows('games')): the_row(); ?>
										 
											<div class="result">
												<div class="time">
													<?php if(get_sub_field('date') !== ''): ?>
														<?php the_sub_field('date'); ?>
													<?php endif; ?>
												</div>

												<div class="match-up">
													<span class="team-a">
														<?php $post_object = get_sub_field('team_a'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
															<a href="<?php the_permalink(); ?>"><?php the_field('school'); ?> (<?php the_field('seed'); ?>)</a>
														<?php wp_reset_postdata(); endif; ?>

													</span>
													<span class="versus"><?php the_sub_field('team_a_score'); ?> - <?php the_sub_field('team_b_score'); ?></span>
													<span class="team-b">
														<?php $post_object = get_sub_field('team_b'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
															<a href="<?php the_permalink(); ?>"><?php the_field('school'); ?> (<?php the_field('seed'); ?>)</a>
														<?php wp_reset_postdata(); endif; ?>
													</span>
											    </div>

												<div class="link">
													<?php if(get_sub_field('field') !== ''): ?>
														Field <?php the_sub_field('field'); ?>
													<?php endif; ?>
												</div>
											</div>

										<?php endwhile; endif; ?>

						    		</div>

								</div>
								
						    <?php endif; ?>
						 
						<?php endwhile; endif; ?>

					</article>

					<article id="bracket">
						<?php if(get_field('score_reporter_link')): ?>
							<a href="<?php the_field('score_reporter_link'); ?>" class="score-reporter">Full Results on Score Reporter</a>
						<?php endif; ?>

						<section id="tournament">

							<?php if(have_rows('prequarterfinals_games')): ?>
							<ul class="round round-1">
								<h5>Prequarters</h5>

								 <?php while(have_rows('prequarterfinals_games')): the_row(); ?>

									<li class="spacer first">&nbsp;</li>
									
									<?php get_template_part('partials/game-bracket'); ?>

									<li class="spacer">&nbsp;</li>

									<li class="game game-top winner empty">Empty</li>
									<li class="game game-spacer empty">&nbsp;</li>
									<li class="game game-bottom empty"&nbsp>Empty</li>

								<?php endwhile; ?>

								<li class="spacer last">&nbsp;</li>

							</ul>

							<?php endif; ?>

							<ul class="round round-2">
								<h5>Quarterfinals</h5>

								<li class="spacer first">&nbsp;</li>
								
								<?php if(have_rows('quarterfinals_games')): while(have_rows('quarterfinals_games')): the_row(); ?>
									
									<?php get_template_part('partials/game-bracket'); ?>

									<li class="spacer">&nbsp;</li>

								<?php endwhile; endif; ?>

							</ul>

							<ul class="round round-3">
								<h5>Semifinals</h5>

								<li class="spacer first">&nbsp;</li>
								
								<?php if(have_rows('semifinals_games')): while(have_rows('semifinals_games')): the_row(); ?>
									
									<?php get_template_part('partials/game-bracket'); ?>

									<li class="spacer">&nbsp;</li>

								<?php endwhile; endif; ?>

							</ul>

							<ul class="round round-4">
								<h5>Finals</h5>

								<li class="spacer first">&nbsp;</li>

								<?php if(have_rows('finals_games')): while(have_rows('finals_games')): the_row(); ?>
									
									<?php get_template_part('partials/game-bracket'); ?>

								<?php endwhile; endif; ?>

								<li class="spacer">&nbsp;</li>

							</ul>		

						</section>

					</article>


				</div>



			</section>


			<section id="teams">

				<h3 class="section-heading">Teams</h3>

				<section id="teams-wrapper">

					<?php
						$teamCat = get_field('category');

						$args = array(
							'post_type' => 'team',
							'posts_per_page' => 30,
							'cat' => $teamCat,
							'order' => 'asc',
							'orderby' => 'title'
						);
						$query = new WP_Query( $args );
						if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

						<div class="team">
							<div class="logo">
								<a href="<?php the_permalink(); ?>"><img src="<?php $image = get_field('logo'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
							</div>

							<div class="info">
								<h3><a href="<?php the_permalink(); ?>"><?php the_field('team_name'); ?></a></h3>
							</div>


						</div>

					<?php endwhile; endif; wp_reset_postdata(); ?>

				</section>

			</section>







		</div>
	</section>

<?php get_footer(); ?>