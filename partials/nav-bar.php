<?php $events = [209, 211, 213]; foreach ($events as $event): ?>

	<?php if ( is_tree($event) || (is_team() && in_team_category($event))  ): ?>

		<nav id="bar">
			<div class="wrapper">


				<h1><a href="<?php the_field('event_homepage', $event); ?>"><?php the_field('event_short_name', $event); ?></a></h1>

				<?php $posts = get_field('event_nav_bar', $event); if( $posts !== null ): ?>

					<a href="#" id="menu-toggle">Menu</a>

					<div class="menu">
						<?php get_template_part('partials/bar/menu'); ?>
					</div>

				<?php wp_reset_postdata(); endif; ?>

			</div>
		</nav>

		<nav id="sub-bar">
			<div class="wrapper">

				<?php $posts = get_field('event_nav_sub_bar', $event); if( $posts !== null ): ?>

					<?php get_template_part('partials/bar/menu'); ?>

				<?php wp_reset_postdata(); endif; ?>

			</div>
		</nav>

	<?php endif; ?>

<?php endforeach; ?>