<li class="game game-top">
	<?php $post_object = get_sub_field('team_a'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
		<a href="<?php the_permalink(); ?>"><?php the_field('team_name'); ?> (<?php the_field('seed'); ?>)</a>
	<?php wp_reset_postdata(); endif; ?>
	<span><?php the_sub_field('team_a_score'); ?></span>
</li>

<li class="game game-spacer">
	<?php $field = get_sub_field('field'); if(is_numeric($field)): ?> 
		<?php the_sub_field('date'); ?> - Field <?php the_sub_field('field'); ?>
	<?php else: ?>
		<?php if(get_sub_field('date')): ?><?php the_sub_field('date'); ?> - <?php endif; ?><?php the_sub_field('field'); ?>
	<?php endif; ?>
</li>

<li class="game game-bottom ">
	<?php $post_object = get_sub_field('team_b'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
		<a href="<?php the_permalink(); ?>"><?php the_field('team_name'); ?> (<?php the_field('seed'); ?>)</a>
	<?php wp_reset_postdata(); endif; ?>
	<span><?php the_sub_field('team_b_score'); ?></span>
</li>