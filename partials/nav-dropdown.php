<nav id="dropdown">
	<div class="wrapper">

		<?php $events = [209, 211, 213];  foreach ($events as $event): ?>

			<div class="col">
				<h4>
					<a href="<?php the_field('event_homepage', $event); ?>">
						<?php the_field('event_short_name', $event); ?>
					</a>
				</h4>

				<?php if(have_rows('event_nav', $event)): ?>

					<ul>
						<?php while(have_rows('event_nav', $event)): the_row(); ?>

							<?php $altLabel = get_sub_field('alt_label'); ?>

							<?php $post_object = get_sub_field('link'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

								<?php if($altLabel): ?>

									<li><a href="<?php the_permalink(); ?>" class="division"><?php echo $altLabel; ?></a> <span class="sep">&middot;</span> <a href="<?php the_permalink(); ?>#schedule" class="sub-link">Schedule & Results</a> <span class="sep">&middot;</span> <a href="<?php the_permalink(); ?>" class="sub-link">Teams</a></li>

								<?php else: ?>

									<li><a href="<?php the_permalink(); ?>" class="division"><?php the_title(); ?></a> <span class="sep">&middot;</span> <a href="<?php the_permalink(); ?>#schedule" class="sub-link">Schedule & Results</a> <span class="sep">&middot;</span> <a href="<?php the_permalink(); ?>" class="sub-link">Teams</a></li>

								<?php endif; ?>
								
							<?php wp_reset_postdata(); endif; ?>

						<?php endwhile; ?>
					</ul>

				<?php endif; ?>
			</div>

		<?php endforeach; ?>

		<div class="col news">
			<h4>
				<a href="<?php echo site_url('/news/'); ?>" class="news">News</a>
			</h4>
		</div>

	</div>
</nav>