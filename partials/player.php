<div class="player">
	<div class="image">
		<a href="<?php the_permalink(); ?>">
			<img src="<?php $image = get_field('headshot'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</a>
	</div>

	<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
</div>