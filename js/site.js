function removeHash () { 
    history.pushState("", document.title, window.location.pathname + window.location.search);
}

$(document).ready(function() {
	


	// rel="external"
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});


	$('.photo img').bind('contextmenu', function(e) {
	    return false;
	});
	
	$('.photo img').on('dragstart', function(event) {
		event.preventDefault();
	});


	$('#toggle').click(function(){

		$('header').toggleClass('open');
		$('#dropdown').slideToggle(300);
		return false;
	});

	$('#menu-toggle').click(function(){
		var menu = $(this).siblings('.menu');
		$(menu).slideToggle(300);
		return false;
	});


	// Homepage Carousel
	$('.page-template-homepage #hero').slick({
		dots: true,
		arrows: false,
		infinite: true,
		speed:  500,
		autoplay: true,
		autoplaySpeed: 6000,
		slidesToShow: 1,
		slidesToScroll: 1,
	});		


	// FAQs
	$('.faq .question').click(function(e) {
  
	    var $this = $(this);
	  
	    if ($this.next().hasClass('show')) {
	        $this.next().removeClass('show');
	        $this.next().slideUp(350);
	    } else {
	        $this.parent().parent().find('.faq .answer').removeClass('show');
	        $this.parent().parent().find('.faq .answer').slideUp(350);
	        $this.next().toggleClass('show');
	        $this.next().slideToggle(350);
		    $('.faq .question').removeClass('active');
	    }

	    $this.toggleClass('active');

		return false;

	});

	// Schedule Tabs
	$('#tab-links a').click(function () {

		var tabs = $('#tabs > article');
		var targetTab = $(this).attr('href');
		tabs.hide().filter(targetTab).show();

		$('#tab-links a').removeClass('active');
		$(this).addClass('active');
		
		return false;

	}).filter(':first').click();


	// Conference Tabs
	$('#conferences-tab-links a').click(function () {

		var tabs = $('#conferences-tabs > div.region');
		var targetTab = $(this).attr('href');
		tabs.hide().filter(targetTab).css('display', 'flex');

		$('#conferences-tab-links a').removeClass('active');
		$(this).addClass('active');
		
		return false;

	}).filter(':first').click();

	// Regionals Tabs
	$('#regionals-tab-links a').click(function () {

		var tabs = $('#regionals-tabs > div.region');
		var targetTab = $(this).attr('href');
		tabs.hide().filter(targetTab).css('display', 'flex');

		$('#regionals-tab-links a').removeClass('active');
		$(this).addClass('active');
		
		return false;

	}).filter(':first').click();


	// Stats 
	$(".tablesorter").tablesorter(); 



	// Video Thumbnail
	$('.thumbnail a').click(function(){
		var loadTarget = $(this).attr('data-video-src') + ' #video-content';

		$.smoothScroll({scrollTarget: '#video-player'});

		$('#video-player .wrapper').empty().load(loadTarget, function() {
			$('#video-player .player, #video-player .info').fadeIn('slow');
			$('.player').fitVids();
		});

		return false;		
	});



	// FitVids
	$('.player').fitVids();


	// Team Tab
	var hash = location.hash;

	if(hash == "#schedule") {
		$('#pool-play-tab').trigger('click');
		removeHash();
	}


});