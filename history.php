<?php

/*

	Template Name: History

*/

get_header(); ?>


  	<?php get_template_part('partials/hero'); ?>

	<section id="main">
		<div class="wrapper">

			<article class="default">

				<div class="content">
					<?php the_field('content'); ?>
				</div>

				<h3>Past Champions</h3>

				<table>
					<thead>
					    <tr>
					        <td class="year">Year</td>
					        <td class="champion">Champion</td>
					        <td class="runner-up">Runner-up</td>
					        <td class="location">Location</td>
					    </tr>
					</thead>

					<tbody>
						<?php if(have_rows('history')): while(have_rows('history')): the_row(); ?>
						 
						    <tr>
						        <td class="year"><?php the_sub_field('year'); ?></td>
						        <td class="champion"><?php the_sub_field('champion'); ?></td>
						        <td class="runner-up"><?php the_sub_field('runner_up'); ?></td>
						        <td class="location"><?php the_sub_field('location'); ?></td>
						    </tr>

						<?php endwhile; endif; ?>
					</tbody>					
				</table>

			</article>

		</div>
	</section>

<?php get_footer(); ?>