	<?php get_template_part('partials/sponsors'); ?>

	<?php get_template_part('partials/media-partners'); ?>

	<footer>
		<div class="wrapper">

			<div class="col logo">
				<a href="http://usaultimate.org/" rel="external">
					<img src="<?php $image = get_field('footer_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>

			<?php $events = [209, 211, 213]; foreach ($events as $event): ?>

				<div class="col">
					<h4>
						<a href="<?php the_field('event_homepage', $event); ?>">
							<?php the_field('event_short_name', $event); ?>
						</a>
					</h4>

					<?php if(have_rows('event_nav', $event)): ?>

						<ul>
							<?php while(have_rows('event_nav', $event)): the_row(); ?>

								<?php $altLabel = get_sub_field('alt_label'); ?>

								<?php $post_object = get_sub_field('link'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
			
									<?php if($altLabel): ?>

										<li><a href="<?php the_permalink(); ?>" class="division"><?php echo $altLabel; ?></a> <span class="sep">&middot;</span> <a href="<?php the_permalink(); ?>#schedule" class="sub-link">Schedule & Results</a> <span class="sep">&middot;</span> <a href="<?php the_permalink(); ?>" class="sub-link">Teams</a></li>

									<?php else: ?>

										<li><a href="<?php the_permalink(); ?>" class="division"><?php the_title(); ?></a> <span class="sep">&middot;</span> <a href="<?php the_permalink(); ?>#schedule" class="sub-link">Schedule & Results</a> <span class="sep">&middot;</span> <a href="<?php the_permalink(); ?>" class="sub-link">Teams</a></li>

									<?php endif; ?>
								

								<?php wp_reset_postdata(); endif; ?>

							<?php endwhile; ?>
						</ul>

					<?php endif; ?>
				</div>

			<?php endforeach; ?>



			<div class="copyright">
				<?php the_field('copyright', 'options'); ?>

				<p class="credits">Site: <a href="http://andrewlovseth.com/">Andrew Lovseth</a></p>
			</div>


		</div>
	</footer>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>
	
	<?php wp_footer(); ?>


	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109668875-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	 
	  gtag('config', 'UA-109668875-1');
	</script>
	 

</body>
</html>