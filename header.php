<!DOCTYPE html>
<html>
<head>

	<title><?php wp_title(); ?></title>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700|Montserrat:400,700|Merriweather:400,400i,700,700i" rel="stylesheet">
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.png" />
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?> id="<?php echo the_slug();?>">

	<section id="utility-nav">
		<div class="wrapper">

			<a href="<?php echo site_url('/news/'); ?>" class="news">News</a>

			<div class="division icc">
				<strong><a href="<?php echo site_url('/icc/'); ?>">ICC</a></strong>
				<a href="<?php echo site_url('/icc/men/'); ?>">Men</a>
				<span class="dot">&middot;</span>
				<a href="<?php echo site_url('/icc/mixed/'); ?>">Mixed</a>
				<span class="dot">&middot;</span>
				<a href="<?php echo site_url('/icc/women/'); ?>">Women</a>
			</div>

			<div class="division ycc-u20">
				<strong><a href="<?php echo site_url('/ycc-u20/'); ?>">YCC U20</a></strong>
				<a href="<?php echo site_url('/ycc-u20/boys/'); ?>">Boys</a>
				<span class="dot">&middot;</span>
				<a href="<?php echo site_url('/ycc-u20/girls/'); ?>">Girls</a>
				<span class="dot">&middot;</span>
				<a href="<?php echo site_url('/ycc-u20/mixed/'); ?>">Mixed</a>
			</div>

			<div class="division ycc-u17">
				<strong><a href="<?php echo site_url('/ycc-u17/'); ?>">YCC U17</a></strong>
				<a href="<?php echo site_url('/ycc-u17/boys/'); ?>">Boys</a>
				<span class="dot">&middot;</span>
				<a href="<?php echo site_url('/ycc-u17/girls/'); ?>">Girls</a>
			</div>

		</div>
	</section>


	<header>
		<div class="wrapper">

			<div class="logo">
				<a href="<?php echo site_url('/'); ?>">
					<img src="<?php $image = get_field('logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>

				<h1><a href="<?php echo site_url('/'); ?>">U.S. Open</a></h1>

			</div>

			<?php if(get_field('show_watch_live', 'options') == true): ?>
				<a href="<?php echo site_url('/watch-live'); ?>" class="watch-live">Watch Live</a>
			<?php endif; ?>

			<a href="#" id="toggle" class="no-translate">
				<div class="patty"></div>
			</a>

		</div>
	</header>

	<?php get_template_part('partials/nav-bar'); ?>

	<?php get_template_part('partials/nav-dropdown'); ?>

